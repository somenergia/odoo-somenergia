# Som Energia customizations of Ecosistema IT Cooperatiu
[![PyPI version](https://badge.fury.io/py/odoo12-addon-somenergia.svg)](https://badge.fury.io/py/odoo12-addon-somenergia)

----

![Logo Ecosistema IT Cooperatiu](https://i0.wp.com/coopdevs.org/wp-content/uploads/2021/07/logo_ecosistema-it-cooperatiu-1.png?resize=1024%2C294&ssl=1)

This repository contains customizations of the project [Ecosistema IT Cooperatiu](https://coopdevs.org/ecosistema-it/), and is an Odoo module for 12.0 version. Is part of the project “Projectes Singulars Covid19” de la Generalitat de Catalunya.

